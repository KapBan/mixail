#include "stdafx.h"
#include <iostream>


class Exam{
public:
    virtual void pass() = 0;
};

class PassTalk: public Exam{
public:
    void pass(){
        std::cout << "Talking exam is successfully comlpeted!";
    };
};

class PassWrite: public Exam{
public:
    void pass(){
        std::cout << "Writing exam is successfully comleted!";
    };
};

class Subj {
protected:
    Exam* PassWay;
public:
    virtual void passEx() = 0;
};

class Math: public Subj {
public:
    Math(Exam* ExKind1){
        PassWay = ExKind1;   
    };
    void passEx() {
        PassWay->pass();
    };
};

class Physics: public Subj {
public:
    Physics(Exam* ExKind1){
        PassWay = ExKind1;
    };
    void passEx() {
        PassWay->pass();
    };
};

class ForeignLang: public Subj {
public:
    ForeignLang(Exam* ExKind1){
        PassWay = ExKind1;
    };
    void passEx() {
        PassWay->pass();
    };
};


int _tmain(int argc, _TCHAR* argv[])
{
    PassTalk* ex1 = new PassTalk();
    PassWrite* ex2 = new PassWrite();
    Math* mtm = new Math(ex2);
    ForeignLang* frnLn = new ForeignLang(ex1);
    frnLn->passEx();
    std::cout << "\n";
    mtm->passEx();
    std::cout << "\n";
    system("PAUSE");
	return 0;
}

